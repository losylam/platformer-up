game.PlayScreen = me.Stage.extend({
    /**
     *  action to perform on state change
     */
    onResetEvent: function() {
      // load a level
        me.levelDirector.loadLevel("map1");

	console.log('play onReset');
	
        // reset the score
        game.data.score = 0;
        // play some music
        me.audio.playTrack("dst-gameforest");
    },

    /**
     *  action to perform on state change
     */
    onDestroyEvent: function() {

        // remove the HUD from the game world
        me.game.world.removeChild(this.HUD);
	
	console.log('play onDestroy');
	
        // remove the joypad if initially added
        if (this.virtualJoypad && me.game.world.hasChild(this.virtualJoypad)) {
          me.game.world.removeChild(this.virtualJoypad);
        }

        // stop some music
        me.audio.stopTrack("dst-gameforest");
    }
});
