game.EchelleEntity = me.Entity.extend({
    /**
     * constructor
     */
    init: function (x, y, settings) {
        // call the super constructor
        this._super(me.Entity, "init", [ x, y, settings ]);


//	this.body.collisionType = me.collision.types.USER;
        // add a physic body
        this.body = new me.Body(this);
        this.body.addShape(new me.Ellipse(this.width / 2, this.height / 2, this.width, this.height))
	this.body.collisionType = me.collision.types.USER;
    },

    /**
     * collision handling
     */
    onCollision : function (/*response*/) {

        return false;
    }
});
