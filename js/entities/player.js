game.PlayerEntity = me.Entity.extend({
    init: function(x, y, settings) {
	console.log('player init');
	
        // call the constructor
        this._super(me.Entity, "init", [x, y , settings]);

	game.data.width = this._width;
	game.data.height = this._height;
	
	
        // player can exit the viewport (jumping, falling into a hole, etc.)
        this.alwaysUpdate = true;

	this.in_echelle = false;
	this.was_in_echelle = false;
	
	this.default_gravity = 0.9;
	this.body.gravity.y = this.default_gravity;
	
        // walking & jumping speed
        this.body.setMaxVelocity(3, 15);
        this.body.setFriction(0.4, 0);

	this.is_moving = false;
	
	this.crouch = false;
	this.was_crouch = false;

        this.dying = false;

        this.mutipleJump = 1;

        // set the viewport to follow this renderable on both axis, and enable damping
//        me.game.viewport.follow(this, me.game.viewport.AXIS.BOTH, 0.1);

        // enable keyboard
        me.input.bindKey(me.input.KEY.LEFT,  "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.X,     "jump", true);
        me.input.bindKey(me.input.KEY.UP,    "up");
        me.input.bindKey(me.input.KEY.SPACE, "crouch");
        me.input.bindKey(me.input.KEY.DOWN,  "down");
	me.input.bindKey(me.input.KEY.ENTER,  "jump", true);

        me.input.bindKey(me.input.KEY.A,     "left");
        me.input.bindKey(me.input.KEY.D,     "right");
        me.input.bindKey(me.input.KEY.W,     "jump", true);
        me.input.bindKey(me.input.KEY.S,     "down");

        me.input.bindKey(me.input.KEY.K,     "kill");

	
//        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.UP}, me.input.KEY.UP);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.FACE_1}, me.input.KEY.ENTER);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.FACE_2}, me.input.KEY.SPACE);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.DOWN}, me.input.KEY.DOWN);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.FACE_3}, me.input.KEY.DOWN);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.FACE_4}, me.input.KEY.DOWN);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.LEFT}, me.input.KEY.LEFT);
        me.input.bindGamepad(0, {type: "buttons", code: me.input.GAMEPAD.BUTTONS.RIGHT}, me.input.KEY.RIGHT);

        // map axes
        me.input.bindGamepad(0, {type:"axes", code: me.input.GAMEPAD.AXES.LX, threshold: 0.5}, me.input.KEY.LEFT);
        me.input.bindGamepad(0, {type:"axes", code: me.input.GAMEPAD.AXES.LX, threshold: -0.5}, me.input.KEY.RIGHT);
        me.input.bindGamepad(0, {type:"axes", code: me.input.GAMEPAD.AXES.LY, threshold: 0.5}, me.input.KEY.UP);
        me.input.bindGamepad(0, {type:"axes", code: me.input.GAMEPAD.AXES.LY, threshold: -0.5}, me.input.KEY.DOWN);

        // set a renderable
        this.renderable = game.texture.createAnimationFromName([
	    "face", "marche_00", "marche_01", "marche_02",
	    "marche_03", "marche_04", "marche_05",
	    "marche_06", "crouch0", "crouch1", "crouch3",
	    "climb0", "climb1", "climb2"
	]);

        // define a basic walking animatin
        this.renderable.addAnimation ("face",  [{ name: "face", delay: 100 }]);

        this.renderable.addAnimation ("walk",  [{ name: "marche_00", delay: 100 },
						{ name: "marche_01", delay: 100 },
						{ name: "marche_02", delay: 100 },
						{ name: "marche_03", delay: 100 },
						{ name: "marche_04", delay: 100 },
						{ name: "marche_05", delay: 100 },
						{ name: "marche_06", delay: 100 }]);

	this.renderable.addAnimation ("crouch",  [{ name: "crouch0", delay: 100 },
						  { name: "crouch1", delay: 100 },
						  { name: "crouch0", delay: 100 },
						  { name: "crouch3", delay: 100 }]);

	this.renderable.addAnimation ("climb",  [{ name: "climb0", delay: 100 },
						 { name: "climb1", delay: 100 },
						 { name: "climb0", delay: 100 },
						 { name: "climb2", delay: 100 }]);

	
        // set as default
        this.renderable.setCurrentAnimation("face");
        // set as default

	
        // set the renderable position to bottom center
        this.anchorPoint.set(0.5, 1.0);

	var background_niveau1 = new me.Sprite(0, 0, {
	    image: me.loader.getImage('map_niveau1'),
        });
	
	background_niveau1.anchorPoint.set(0, 0);
	background_niveau1.alpha = 0;
	background_niveau1.pos._x = -10;
	background_niveau1.pos._y = -10;
	
	me.game.world.addChild(background_niveau1, 1);
	
	var background_niveau2 = new me.Sprite(0, 0, {
	    image: me.loader.getImage('map_niveau2'),
        });
	
	background_niveau2.anchorPoint.set(0, 0);
	background_niveau2.alpha = 0;
	background_niveau2.pos._x = -10;
	background_niveau2.pos._y = -10;
	
	me.game.world.addChild(background_niveau2, 1);
	
	var background_niveau3 = new me.Sprite(0, 0, {
	    image: me.loader.getImage('map_niveau3'),
        });
	
	background_niveau3.anchorPoint.set(0, 0);
	background_niveau3.alpha = 0;
	background_niveau3.pos._x = -10;
	background_niveau3.pos._y = -10;
	
	    
	me.game.world.addChild(background_niveau3, 1);
	
	
	game.data.niveau1 = background_niveau1;
	game.data.niveau2 = background_niveau2;
	game.data.niveau3 = background_niveau3;

    },

    /* -----

        update the player pos

    ------            */
    update : function (dt) {

	game.data.niveau1.alpha = Math.max(game.data.niveau1.alpha - 0.04, 0);
	game.data.niveau2.alpha = Math.max(game.data.niveau2.alpha - 0.04, 0);
	game.data.niveau3.alpha = Math.max(game.data.niveau3.alpha - 0.04, 0);
	
	if(this.in_echelle && !this.was_in_echelle){
	    this.was_in_echelle = true;
	    //this.renderable.setCurrentAnimation('climb');
	}else if (this.in_echelle){
	    this.in_echelle = false;
	    this.body.gravity.y = 0;
	    this.body.setFriction(0.4, 0.7);
	}else{
	    if (this.was_in_echelle){
		//this.renderable.setCurrentAnimation('face');
		this.body.gravity.y = this.default_gravity;
		this.body.setFriction(0.4, 0);
		this.was_in_echelle = false;
	    }
	}
	
        if (me.input.isKeyPressed("left"))    {
            this.body.force.x = -this.body.maxVel.x;
            this.renderable.flipX(true);
	    this.is_moving = true;
        } else if (me.input.isKeyPressed("right")) {
            this.body.force.x = this.body.maxVel.x;
            this.renderable.flipX(false);
	    this.is_moving = true;
        } else {
            this.body.force.x = 0;
	    this.is_moving = false;
        }

        if (me.input.isKeyPressed("up")) {
	    if (!this.body.jumping){
		this.body.force.y = -this.body.maxVel.x/4;
	    }
	}else if (me.input.isKeyPressed("down")){
	    this.body.force.y = this.body.maxVel.x/4;
	}else{
	    this.body.force.y = 0;
	}
	
        if (me.input.isKeyPressed("jump") && !me.input.isKeyPressed("up")) {
            this.body.jumping = true;
            if (this.multipleJump <= 2) {
                // easy "math" for double jump
                this.body.force.y = -this.body.maxVel.y * this.multipleJump++;
//                me.audio.play("jump", false);
            }
        }
        else {

//            this.body.force.y = 0;

            if (!this.body.falling && !this.body.jumping) {
                // reset the multipleJump flag if on the ground
                this.multipleJump = 1;
            }
            else if (this.body.falling && this.multipleJump < 2) {
                // reset the multipleJump flag if falling
                this.multipleJump = 2;
            }
        }
	
	if (me.input.isKeyPressed("crouch")){
	    if (!this.crouch){
		this.crouch = true;
		this.body.shapes = [];
		this.body.addShape(new me.Rect(0, this.height/2, this.width, this.height/2));
		//this.renderable.setCurrentAnimation("crouch");
	    }
	}else{
	    if (this.crouch && !this.was_crouch){
		this.was_crouch = true;
		this.crouch = false;
		this.body.shapes = [];
		this.body.addShape(new me.Rect(0, 0, this.width, this.height));
		//this.renderable.setCurrentAnimation("walk");
	    }else{
		this.was_crouch = false;
	    }
	    
	}
	
	// apply physics to the body (this moves the entity)
        this.body.update(dt);


	if (me.input.isKeyPressed("kill")){
            me.game.world.removeChild(this);
            me.game.viewport.fadeIn("#fff", 150, function(){
		//                me.audio.play("die", false);
                me.levelDirector.reloadLevel();
                me.game.viewport.fadeOut("#000", 300);

            });
	}
	
        // check if we fell into a hole
        if (!this.inViewport && (this.pos.y > me.video.renderer.getHeight())) {
            // if yes reset the game
            me.game.world.removeChild(this);
            me.game.viewport.fadeIn("#fff", 150, function(){
                me.audio.play("die", false);
                me.levelDirector.reloadLevel();
                me.game.viewport.fadeOut("#000", 300);
            });
            return true;
        }

        // handle collisions against other shapes
        me.collision.check(this);


	if (this.in_echelle){
	    if (!this.renderable.isCurrentAnimation('climb')){
		this.renderable.setCurrentAnimation('climb')
	    }
	}else{
	    if (this.crouch){
		if (!this.renderable.isCurrentAnimation('crouch')){
		    this.renderable.setCurrentAnimation('crouch')
		}		
	    }else{
		if (this.is_moving){
		    if (!this.renderable.isCurrentAnimation('walk')){
			this.renderable.setCurrentAnimation('walk')
		    }
		}else{
		    if (!this.renderable.isCurrentAnimation('face')){
			this.renderable.setCurrentAnimation('face')
		    }
		}
	    }
	}
	
        // check if we moved (an "idle" animation would definitely be cleaner)
        if (this.body.vel.x !== 0 || this.body.vel.y !== 0 ||
            (this.renderable && this.renderable.isFlickering())
        ) {
            this._super(me.Entity, "update", [dt]);
            return true;
        }

	game.data.position = this.pos;
	
        return false;
    },


    /**
     * colision handler
     */
    onCollision : function (response, other) {
        switch (other.body.collisionType) {
        case me.collision.types.WORLD_SHAPE:
	    if (response.overlapV.y < 0){
		if (this.was_crouch){
		    this.crouch = true;
		    this.body.shapes = [];
		    this.body.addShape(new me.Rect(0, this.height/2, this.width, this.height/2));
		    this.renderable.setCurrentAnimation("crouch");
		}
	    }
	    
            // Simulate a platform object
            if (other.type === "platform") {
                    if (this.body.falling &&
                        !me.input.isKeyPressed("down") &&
                        // Shortest overlap would move the player upward
                        (response.overlapV.y > 0) &&
                        // The velocity is reasonably fast enough to have penetrated to the overlap depth
                        (~~this.body.vel.y >= ~~response.overlapV.y)
                    ) {
                        // Disable collision on the x axis
                        response.overlapV.x = 0;
                        // Repond to the platform (it is solid)
                        return true;
		    }
		// Do not respond to the platform (pass through)
                    return false;
                }

                // Custom collision response for slopes
                else if (other.type === "slope") {
                    // Always adjust the collision response upward
                    response.overlapV.y = Math.abs(response.overlap);
                    response.overlapV.x = 0;

                    // Respond to the slope (it is solid)
                    return true;
                }
                break;

	case me.collision.types.USER:
	    this.in_echelle = true;
	    return false;
	    break;
	    

	case me.collision.types.USER << 1:

	    return false;
	    break;
            default:
                // Do not respond to other objects (e.g. coins)
                return false;
        }

        // Make the object solid
        return true;
    },
});
