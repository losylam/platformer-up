game.PlotEntity = me.Entity.extend({
    /**
     * constructor
     */
    init: function (x, y, settings) {
	console.log('plot init');
	
        // call the super constructor
        this._super(me.Entity, "init", [ x, y, settings ]);

	this.not_showed = true;
	
	this.settings = settings;

//	this.body.collisionType = me.collision.types.USER;
        // add a physic body
        this.body = new me.Body(this);
        this.body.addShape(new me.Ellipse(this.width / 2, this.height / 2, this.width, this.height))
	this.body.collisionType = me.collision.types.USER << 1;
    },

    /**
     * collision handling
     */
    onCollision : function (/*response*/) {
	if (game.data[this.settings.niveau].alpha < 1){
	    game.data[this.settings.niveau].alpha += 0.06;
	}
	
        return false;
    }
});
