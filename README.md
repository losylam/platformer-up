Platformer, version UP!
=======

L'exemple platformer de melonJS, modifié pour le festival UP

La Soufflerie + Social Gamer Club + Organic Orchestra

Installation de melonJS
-------

D'après le README.md de melonJS :

    $ [sudo] npm install -g grunt-cli
    $ git clone https://github.com/melonjs/melonJS.git
    $ cd melonJS
    $ npm install
    $ grunt
 
Pour lancer le server et accéder au exemples :

    $ grunt serve
 
Puis ouvrir un navigateur à l'adresse localhost:8000 pour accéder aux exemples

Utilisation de la version modifiée
--------

Pour l'instant, nous utilisons la version de test de melonJS installée d'après la procédure ci-dessus.

Pour remplacer l'exemple platformer, remplacer le dossier platformer dans l'arborescence de melonJS par celui-ci.

Description des fichiers de Platformer
--------

    - data                  -> fichiers medias (son, textures, cartes...)
    - js/resources.js       -> liens vers les fichiers de médias (son, textures, cartes...)
    - js/games.js           -> initialisation du jeu
    - js/screens/plays.js   -> affichage global de la scène play (la seule scène)
    - js/entities/player.js -> le comportement du personnage

Modification de la carte
-------

On peut ouvrir le fichier data/map/map1.tmx avec le logiciel [tiled](https://www.mapeditor.org/).

Modification du personnage
-------

Les sources (dessins scannés, pseudo-animation vectorielles) sont dans le dossier:
    
	data/img/Up
	
Le dossier contient également les sprites exportés dans leur répertoire.

Pour exporter les sprites, nous utilisons le logiciel [texturepacker](https://www.codeandweb.com/texturepacker). Le logiciel permet d'exporter la texture (up_texture.png) et les repères (up_texture.json)

La version de démo ne permet pas de définir la position du point pivot. Il faut l'ajouter pour chaque image dans le fichier up_texture.json
